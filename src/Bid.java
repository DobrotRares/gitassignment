

public  class Bid {
  private final String user;
  private final int value;

  public Bid(String user, int value) {
    this.user = user;
    this.value = value;
  }

  public String getUser() {
    return user;
  }

  public int getValue() {
    return value;
  }

  

public void displayInformationsAboutAuctioneer(){
	System.out.println("User:"+this.user+"Value:"+this.value);
}
		
}
